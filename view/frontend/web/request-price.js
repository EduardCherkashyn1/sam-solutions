define([
    'jquery',
    'Magento_Ui/js/modal/modal'
], function ($, modal) {
    return function (config) {
        let options = {
            type: 'popup',
            responsive: true,
            innerScroll: true,
            title: 'Request price',
            buttons: [{
                text: $.mage.__('Close'),
                class: '',
                click: function () {
                    this.closeModal();
                }
            }]
        };

        let popup = modal(options, $('#popup-modal'));
        let submitUrl = config.requestPriceUrl;
        $('#request-price').click(function () {
            $('#popup-modal').modal('openModal');
            $('#form_request_price').submit(function (e) {
                e.preventDefault();
                $.ajax({
                    url: submitUrl,
                    type:"POST",
                    dataType: "json",
                    data: $('#form_request_price').serialize(),
                success: function() {
                    alert('Your request was successful!');
                    $('#popup-modal').modal('closeModal');
                    $('#request-price').prop('disabled', true);
                },
                error: function () {
                    alert('Something went wrong!Try again!');
                }
            });
            })
        });
    }
});
