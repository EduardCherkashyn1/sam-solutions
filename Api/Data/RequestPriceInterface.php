<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-08
 * Time: 16:05
 */

namespace EduardCherkashyn\SamSolutions\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;


interface RequestPriceInterface extends ExtensibleDataInterface
{
    /**
     * @return int
     */
    public function getRequestId();

    /**
     * @return string
     */
    public function getUserName();

    /**
     * @param string $name
     * @return void
     */
    public function setUserName($name);

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param string $email
     * @return void
     */
    public function setEmail($email);

    /**
     * @return string
     */
    public function getComment();

    /**
     * @param string $comment
     * @return void
     */
    public function setComment($comment);

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @param string $status
     * @return void
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getProductSku();

    /**
     * @param string $product_id
     * @return void
     */
    public function setProductSku($product_sku);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $created_at
     * @return void
     */
    public function setCreatedAt($created_at);
}

