<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-08
 * Time: 16:16
 */

namespace EduardCherkashyn\SamSolutions\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;


interface RequestPriceSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \EduardCherkashyn\SamSolutions\Api\Data\RequestPriceInterface[]
     */
    public function getItems();

    /**
     * @param \EduardCherkashyn\SamSolutions\Api\Data\RequestPriceInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
