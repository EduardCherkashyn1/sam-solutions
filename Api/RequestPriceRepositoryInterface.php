<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-08
 * Time: 15:55
 */

namespace EduardCherkashyn\SamSolutions\Api;

use EduardCherkashyn\SamSolutions\Api\Data\RequestPriceInterface;
use Magento\Framework\Api\SearchCriteriaInterface;


interface RequestPriceRepositoryInterface
{

    /**
     * @param int $id
     * @return \EduardCherkashyn\SamSolutions\Api\Data\RequestPriceInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param \EduardCherkashyn\SamSolutions\Api\Data\RequestPriceInterface $request
     * @return \EduardCherkashyn\SamSolutions\Api\Data\RequestPriceInterface
     */
    public function save(RequestPriceInterface $request);

    /**
     * @param \EduardCherkashyn\SamSolutions\Api\Data\RequestPriceInterface $request
     * @return void
     */
    public function delete(RequestPriceInterface $request);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \EduardCherkashyn\SamSolutions\Api\Data\RequestPriceSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
