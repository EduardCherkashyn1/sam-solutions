<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-08
 * Time: 15:34
 */

namespace EduardCherkashyn\SamSolutions\Controller\Adminhtml\RequestPrice;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Save extends Action implements HttpPostActionInterface
{
    /**
     * @var \EduardCherkashyn\SamSolutions\Model\RequestPriceRepository
     */
    private $requestPriceRepository;

    /**
     * @var \EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPriceFactory
     */
    private $resourceModel;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;

    /**
     * Save constructor.
     * @param \EduardCherkashyn\SamSolutions\Model\RequestPriceRepository $requestPriceRepository
     * @param \EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPriceFactory $resourceModel
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param Action\Context $context
     */
    public function __construct
    (
        \Magento\Backend\App\Action\Context $context,
        \EduardCherkashyn\SamSolutions\Model\RequestPriceRepository $requestPriceRepository,
        \EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPriceFactory $resourceModel,
        \Magento\Catalog\Model\ProductRepository $productRepository

    ){
        $this->requestPriceRepository = $requestPriceRepository;
        $this->resourceModel = $resourceModel;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $originalRequestData = $this->getRequest()->getPostValue();
        $requestId = $originalRequestData['request_price']['request_id'];
        $sku = $originalRequestData['request_price']['product_sku'];
        $resultRedirect = $this->resultRedirectFactory->create();

        if (!$requestId) {
            $this->messageManager->addErrorMessage(__('Your price request has  not been edited!'));

            return $resultRedirect->setPath('requestprice/requestprice/index');
        }

        if(!$this->validateSKU($sku)) {

            return $resultRedirect->setPath('requestprice/requestprice/edit', ['request_id' => $requestId]);
        }
        try {
            $requestEntity = $this->requestPriceRepository->getById($requestId);
            $resourceModel = $this->resourceModel->create();
            $resourceModel->save($requestEntity->setData($originalRequestData['request_price']));
            $resultRedirect->setPath('requestprice/requestprice/index');
            $this->messageManager->addSuccessMessage(__('Your price request has been edited!'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Product doesn\'t exist anymore!'));

            return $resultRedirect->setPath('requestprice/requestprice/index');
        }

        return $resultRedirect;
    }

    private function validateSKU($sku) :bool
    {
        try {
            $this->productRepository->get($sku);
        } catch (\Exception $e){
            $this->messageManager->addErrorMessage(__('SKU doesn\'t exist!'));

            return false;
        }

        return true;
    }

}
