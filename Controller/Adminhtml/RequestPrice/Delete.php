<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-09
 * Time: 14:28
 */

namespace EduardCherkashyn\SamSolutions\Controller\Adminhtml\RequestPrice;

use Magento\Backend\App\Action;

class Delete extends Action
{
    /**
     * @var \EduardCherkashyn\SamSolutions\Model\RequestPriceRepository
     */
    private $requestPriceRepository;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param \EduardCherkashyn\SamSolutions\Model\RequestPriceRepository $requestPriceRepository
     */
    public function __construct(
        Action\Context $context,
        \EduardCherkashyn\SamSolutions\Model\RequestPriceRepository $requestPriceRepository
    ){
        $this->requestPriceRepository = $requestPriceRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->requestPriceRepository->getById($id);
                $this->requestPriceRepository->delete($model);
                $this->messageManager->addSuccessMessage(__('Your price request has been deleted !'));

                return $resultRedirect->setPath('requestprice/requestprice/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());

                return $resultRedirect->setPath('requestprice/requestprice/edit', ['request_id' => $id]);
            }

        }
        $this->messageManager->addErrorMessage(__('We can\'t find a price request to delete.'));

        return $resultRedirect->setPath('requestprice/requestprice/index');
    }
}
