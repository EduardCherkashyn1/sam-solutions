<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-06
 * Time: 19:57
 */

namespace EduardCherkashyn\SamSolutions\Controller\Index;


use EduardCherkashyn\SamSolutions\Model\RequestPrice;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Catalog\Model\Session
     */
    private $catalogSession;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $resultJsonFactory;
    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    private $sessionManager;
    /**
     * @var \EduardCherkashyn\SamSolutions\Model\RequestPriceFactory
     */
    private $requestPriceFactory;
    /**
     * @var \EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPriceFactory
     */
    private $resourceModel;
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;

    /**
     * Index constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Session\SessionManagerInterface $sessionManager
     * @param \EduardCherkashyn\SamSolutions\Model\RequestPriceFactory $requestPriceFactory
     * @param \EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPriceFactory $resourceModel
     * @param \Magento\Catalog\Model\Session $catalogSession
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context
        ,\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Session\SessionManagerInterface $sessionManager,
        \EduardCherkashyn\SamSolutions\Model\RequestPriceFactory $requestPriceFactory,
        \EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPriceFactory $resourceModel,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        parent::__construct($context);
        $this->catalogSession = $catalogSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sessionManager = $sessionManager;
        $this->requestPriceFactory = $requestPriceFactory;
        $this->resourceModel = $resourceModel;
        $this->productRepository = $productRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function execute() {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultJsonFactory->create();

        if(!$post = $this->validatePost($this->getRequest()->getPost())) {
            return $result->setStatusHeader(400);
        }

        $productId = $this->catalogSession->getData('last_viewed_product_id');

        try{
            $productSku = $this->productRepository->getById($productId)->getSku();
        }catch (\Magento\Framework\Exception\NoSuchEntityException $e){
            return $result->setStatusHeader(400,null,$e->getMessage());
        }


        if(!$this->checkIfAlreadyMadeRequest($productId)) {
            return $result->setStatusHeader(200);
        }
        $requestPrice = $this->requestPriceFactory->create();
        $requestPrice->setUserName($post['request_price_name']);
        $requestPrice->setEmail($post['request_price_email']);
        $requestPrice->setComment($post['request_price_comment']);
        $requestPrice->setStatus(RequestPrice::STATUS_NEW);
        $requestPrice->setProductSku($productSku);

        $resourceModel = $this->resourceModel->create();

        $resourceModel->save($requestPrice);

        return $result->setData($post);
    }

    private function validatePost($post)
    {
        if(!isset($post) || empty($post['request_price_name']) || empty($post['request_price_email'])) {
            return null;
        }

        return $post;
    }

    private function checkIfAlreadyMadeRequest($productId) :bool
    {
        $this->sessionManager->start();
        if(!$madeRequests = $this->sessionManager->getMadeRequest()) {
            $this->sessionManager->setMadeRequest([$productId]);
        } elseif (in_array($productId, $madeRequests)) {
            return false;
        } else {
            $madeRequests[] = $productId;
            $this->sessionManager->setMadeRequest($madeRequests);
        }

        return true;
    }
}
