<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-08
 * Time: 16:19
 */

namespace EduardCherkashyn\SamSolutions\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use EduardCherkashyn\SamSolutions\Api\RequestPriceRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use EduardCherkashyn\SamSolutions\Api\Data\RequestPriceInterface;

class RequestPriceRepository implements RequestPriceRepositoryInterface
{
    private $requestPriceFactory;

    private $collectionFactory;

    public function __construct(
       \EduardCherkashyn\SamSolutions\Model\RequestPriceFactory $requestPriceFactory,
        \EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPrice\CollectionFactory $collectionFactory
    ){
        $this->requestPriceFactory = $requestPriceFactory;
        $this->collectionFactory = $collectionFactory;
    }

    public function getById($id)
    {
        $requestPrice = $this->requestPriceFactory->create();
        $requestPrice->getResource()->load($requestPrice, $id);
        if (! $requestPrice->getId()) {
            throw new NoSuchEntityException(__('Unable to find requestPrice with ID "%1"', $id));
        }
        return $requestPrice;
    }

    public function save(RequestPriceInterface $requestPrice)
    {
        $requestPrice->getResource()->save($requestPrice);
        return $requestPrice;
    }

    public function delete(RequestPriceInterface $requestPrice)
    {
        $requestPrice->getResource()->delete($requestPrice);
    }

    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();

        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);
    }

}
