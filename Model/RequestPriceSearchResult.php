<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-08
 * Time: 16:38
 */

namespace EduardCherkashyn\SamSolutions\Model;

use EduardCherkashyn\SamSolutions\Api\Data\RequestPriceSearchResultInterface;
use Magento\Framework\Api\SearchResults;

class RequestPriceSearchResult extends SearchResults implements RequestPriceSearchResultInterface
{

}
