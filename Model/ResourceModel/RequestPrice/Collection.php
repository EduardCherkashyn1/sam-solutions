<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-06
 * Time: 20:55
 */

namespace EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPrice;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'request_id';
    protected $_eventPrefix = 'EduardCherkashyn_SamSolutions_RequestPrice_collection';
    protected $_eventObject = 'RequestPrice_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('EduardCherkashyn\SamSolutions\Model\RequestPrice', 'EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPrice');
    }

}
