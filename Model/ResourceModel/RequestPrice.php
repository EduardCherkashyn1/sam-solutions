<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-06
 * Time: 20:49
 */

namespace EduardCherkashyn\SamSolutions\Model\ResourceModel;


class RequestPrice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('request_price', 'request_id');
    }

}
