<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-06
 * Time: 20:49
 */

namespace EduardCherkashyn\SamSolutions\Model;

/**
 * Class RequestPrice
 * @package EduardCherkashyn\SamSolutions\Model
 */
class RequestPrice extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,\EduardCherkashyn\SamSolutions\Api\Data\RequestPriceInterface
{
    const STATUS_NEW = 'New';

    const STATUS_IN_PROGRESS = 'In Progress';

    const STATUS_CLOSED = 'Closed';

    const CACHE_TAG = 'EduardCherkashyn_SamSolutions_RequestPrice';

    protected $_cacheTag = 'EduardCherkashyn_SamSolutions_RequestPrice';

    protected $_eventPrefix = 'EduardCherkashyn_SamSolutions_RequestPrice';

    protected function _construct()
    {
        $this->_init('EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPrice');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    public function getRequestId() {
        return $this->_getData('request_id');
    }

    public function getUserName()
    {
        return $this->_getData('user_name');
    }

    public function setUserName($name)
    {
        return $this->setData('user_name', $name);
    }

    public function getEmail()
    {
        return $this->_getData('email');
    }

    public function setEmail($email)
    {
        return $this->setData('email', $email);
    }

    public function getComment()
    {
        return $this->_getData('comment');
    }

    public function setComment($comment)
    {
        return $this->setData('comment', $comment);
    }

    public function getStatus()
    {
        return $this->_getData('status');    }

    public function setStatus($status)
    {
        return $this->setData('status', $status);
    }

    public function getProductSku()
    {
        return $this->_getData('product_sku');    }

    public function setProductSku($product_sku)
    {
        return $this->setData('product_sku', $product_sku);
    }

    public function getCreatedAt()
    {
        return $this->_getData('created_at');    }

    public function setCreatedAt($created_at)
    {
        return $this->setData('created_at', $created_at);
    }
}
