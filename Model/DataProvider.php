<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-07
 * Time: 21:40
 */

namespace EduardCherkashyn\SamSolutions\Model;

use EduardCherkashyn\SamSolutions\Model\ResourceModel\RequestPrice\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $requestPriceCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $requestPriceCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $requestPriceCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = [];

        foreach ($items as $request) {
            $this->loadedData[$request->getId()]['request_price'] = $request->getData();
        }


        return $this->loadedData;
    }
}
