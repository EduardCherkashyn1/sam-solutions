<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2020-08-08
 * Time: 17:28
 */

namespace EduardCherkashyn\SamSolutions\Model\RequestPrice\AttributeSet;


class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => \EduardCherkashyn\SamSolutions\Model\RequestPrice::STATUS_NEW,
                'label' => 'New'
            ],
            [
                'value' => \EduardCherkashyn\SamSolutions\Model\RequestPrice::STATUS_IN_PROGRESS,
                'label' => 'In Progress'
            ],
            [
                'value' => \EduardCherkashyn\SamSolutions\Model\RequestPrice::STATUS_CLOSED,
                'label' => 'Closed'
            ]
        ];
    }
}
